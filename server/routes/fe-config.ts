import { readFile } from 'node:fs/promises';
import { join } from 'node:path';

export default defineEventHandler(async (event) => {
    const configFilePath = join(process.cwd(), '.output', 'public', 'config.json');
    try {
      const configData = await readFile(configFilePath, 'utf-8');
      setResponseHeader(event, 'Content-Type', 'application/json');
      return configData;
} catch (error) {
      throw createError({
        statusCode: 404,
        statusMessage: 'Config file not found',
      });  
    }    
})