export default defineNuxtPlugin(async () => {
  const { data: config } = await useFetch("/fe-config");
  
  return {
    provide: {
      runtimeConfig: config.value,
    },
  };
});
