import { defineNuxtConfig } from "nuxt/config";
import runtimeConfig from './config.json';

export default defineNuxtConfig({
  ssr: false,
  spaLoadingTemplate: 'spa-loading-template.html',
  css: ["~/assets/global.scss"],
  modules: [
    "@ant-design-vue/nuxt",
    "@pinia/nuxt",
    "@nuxtjs/i18n",
    "@vueuse/nuxt"
  ],
  i18n: {    
    legacy: false,
    // lazy: true,
    // strategy: "no_prefix",
    strategy: "prefix_except_default",
    defaultLocale: "ru-Ru",
    langDir: "locales",    
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: 'language',
      redirectOn: 'root' // recommended
    },
    locales: [
      {
        code: "ru-Ru",
        iso: "ru-Ru",
        name: "Russian",
        file: "ru.json",
      },
      {
        code: "en-US",
        iso: "en-US",
        name: "English",
        file: "en.json",
      },      
      {
        code: "es-ES",
        iso: "es-ES",
        name: "Spanish",
        file: "es.json",
      },      
      {
        code: "pt-Pt",
        iso: "pt-Pt",
        name: "Portuguese",
        file: "pt.json",
      },   
      {
        code: "fr-Fr",
        iso: "fr-Fr",
        name: "French",
        file: "fr.json",
      },    
    ],
  },
  router: {
    options: {
      strict: false,
    },
  },
  app: {
    head: {
      link: [
        { rel: 'icon', type: 'image/x-icon', href: `${runtimeConfig.LMA_SERVICE_PATH.length ? '/' + runtimeConfig.LMA_SERVICE_PATH : '' }/favicon.ico` }
      ]
    },
    baseURL: `/${runtimeConfig.LMA_SERVICE_PATH}`
  },
});
