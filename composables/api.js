import { notification } from "ant-design-vue";

export const apiFetch = (request, body) => {
  const { $runtimeConfig } = useNuxtApp();
  const activeDomain = useCookie("domain");
  const activeLanguage = useCookie("language");
  const getLang = (code) => {
    return {
      "ru-Ru": "ru",
      "en-US": "en",
      "es-ES": "es",
      "pt-Pt": "pt",
      "fr-Fr": "fr",
    }[code];
  };
  const requestParams = computed(() => {
    let object = {};
    if (activeDomain.value) {
      object.sess_domain = activeDomain.value.sys_name;
      // object.lang = "ru";
      object.lang = getLang(activeLanguage.value);
    }
    return {
      ...object,
      params: {
        ...body?.value,
      },
    };
  });
  return useFetch(
    request,
    {
      baseURL: process.dev
        ? "https://adviser.lmsoftware.ru/dss/api/v1/"
        : $runtimeConfig.LMA_API_URL
        ? $runtimeConfig.LMA_API_URL
        : `${location.origin}/dss/api/v1/`,
      // baseURL: process.dev
      //   ? "https://adviser.lmsoftware.ru/dss/api/v1/"
      //   : config.public.baseURL
      //   ? config.public.baseURL
      //   : `${location.origin}/dss/api/v1/`,
      // baseURL: process.dev
      //   ? config.public.baseURL
      //   : `${location.origin}/dss/api/v1/`,
      credentials: "include",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: requestParams,
      onError: (error) => console.log("error", error),
      onRequest({ request, options }) {},
      onRequestError({ request, options, error }) {
        console.log(error);
      },
      onResponse({ request, response, options }) {
        return response._data;
      },
      onResponseError({ request, response, options }) {
        if (response.status === 401 || response.status === 302) {
          navigateTo(
            process.dev
              ? "https://sso.adviser.lmsoftware.ru"
              : $runtimeConfig.LMA_SSO_LOGIN_URL
              ? $runtimeConfig.LMA_SSO_LOGIN_URL
              : `https://sso.${location.host}`,
            // process.dev
            //   ? `https://sso.${config.public.hostName}`
            //   : `https://sso.${location.host}`,
            {
              external: true,
            }
          );
        }
        if (response._data.error.code === 4) {
          notification.open({
            message: "Ошибка",
            description: "Не хватает передаваемых параметров",
            style: {
              border: "1px solid #ffa39e",
              backgroundColor: "#fff1f0",
              duration: 5,
            },
          });
        }
        if (response._data.error.code === 5) {
          notification.open({
            message: "Ошибка",
            description: "Ошибка сервера",
            style: {
              border: "1px solid #ffa39e",
              backgroundColor: "#fff1f0",
              duration: 5,
            },
          });
        }
        if (response.status === 500) {
          notification.open({
            message: response._data.error.name,
            description: response._data.error.message,
            style: {
              border: "1px solid #ffa39e",
              backgroundColor: "#fff1f0",
              duration: 5,
            },
          });
        } else {
          console.log(response);
        }
      },
    },
    { watch: [body?.value] }
  );
};
