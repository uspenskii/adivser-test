import { defineStore } from "pinia";
import { notification } from "ant-design-vue";

export const useImpulsesStore = defineStore("impulses-store", {  
  state: () => ({
    responseData: [],
    editedData: [],
    impulsesEditable: false,
    isLoading: false,
  }),
  getters: {},
  actions: {
    add(data) {
      const arr = data.value.data.map((element) => JSON.stringify(element));
      arr.forEach((element) => {
        this.responseData.push(JSON.parse(element));
      });
      this.editedData = data.value.data;
    },
    cancel() {
      this.editedData = [];
      const arr = this.responseData.map((element) => JSON.stringify(element));
      arr.forEach((element) => {
        this.editedData.push(JSON.parse(element));
      });
    },
    clear() {
      this.responseData = [];
      this.editedData = [];
      this.impulsesEditable = false;
    },
    save() {
      const { $event } = useNuxtApp();
      
      this.isLoading = true;      

      let diffArr = [];
      this.editedData.forEach((element, i) => {        
        if (
          element.impulse_value != null &&
          element.impulse_value != this.responseData[i].impulse_value
        ) {
          diffArr.push(element);
        }
      });          

      diffArr.forEach((element) => {
        apiFetch(`set_forecast_param`, {
          value: {
            i_impulse_dt: element.dt,
            i_forecast_id: element.forecast_id,
            i_impulse_type: 1,
            i_impulse_value: element.impulse_value == '' ? null : Number(element.impulse_value),
            i_indr_id: element.indr_id,
          },
        });
      });

      this.impulsesEditable = false;
      this.responseData = [];
      this.editedData = [];
      
      setTimeout(() => {
        if (!this.impulsesEditable) {
          const { $i18n } = useNuxtApp();
          const t = $i18n.t;
          $event("forecasts:update");
          this.isLoading = false;
          notification.open({
            message: t('store.impulses-success'),
            style: {
              border: "1px solid #b7eb8f",
              backgroundColor: "#f6ffed",
            },
          });
        }
      }, 2000);
    },
  },
});
