import { defineStore } from "pinia";

export const useDictionariesStore = defineStore("dictionaries-store", {
  state: () => ({
    dictionaries: [],
  }),
  getters: {},
  actions: {
    async setDictionaries() {
      await apiFetch("get_cls").then((response) => {
        this.dictionaries = response.data
      });
    },
  },
});
