import { defineStore } from "pinia";

export const useSelectorsStore = defineStore("selectors-store", {
  state: () => ({
    groups: [],
    spheres: [],
    sources: [],
    modelGroups: [],
  }),
  getters: {},
  actions: {
    async setSelectors() {
      const req = [
        apiFetch("get_indr_group_tree"),
        apiFetch("get_indr_sphere_tree"),
        apiFetch("get_data_sources_cls", {value: { limit: 100 }}),
        apiFetch("get_model_groups_tree", {value: { limit: 100 }}),
      ]
      const [groups, spheres, sources, modelGroups] = await Promise.all(req)
      this.groups = groups
      this.spheres = spheres
      this.sources = sources
      this.modelGroups = modelGroups
    },
  },
});