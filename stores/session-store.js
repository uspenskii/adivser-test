import { defineStore } from "pinia";

export const useSessionStore = defineStore("session-store", {
  state: () => ({
    user: {},
  }),
  getters: {},
  actions: {},
});
