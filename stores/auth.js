import { defineStore } from "pinia";

export const useAuthStore = defineStore("auth", {
  state: () => ({
    isLoggedIn: false,
    channel: null,
  }),
  actions: {
    initialize() {
      // Инициализация канала
      this.channel = new BroadcastChannel("auth_channel");

      // Обработчик сообщений
      this.channel.onmessage = (event) => {
        if (event.data.type === "LOGOUT") {
          this.handleLogout();
        }
      };
    },

    logout() {
      this.channel?.postMessage({ type: "LOGOUT" });
    },

    handleLogout() {
      const { $runtimeConfig } = useNuxtApp();
      if (process.client) {
        let url =
          $runtimeConfig.LMA_SSO_LOGIN_URL +
          "/login?rd=https%3A%2F%2F" +
          location.host +
          location.pathname +
          "&rm=GET";
        setTimeout(() => {
          navigateTo(url, {
            external: true,
          });
        }, 3000);
      }
    },
  },
});
