import { defineStore } from "pinia";
import { notification } from "ant-design-vue";

export const useJobsStore = defineStore("jobs-store", {
  state: () => ({
    responseData: [],
    editedData: [],
    impulsesEditable: false,
    isLoading: false,
  }),
  getters: {},
  actions: {
    add(data) {
      const arr = data.value.data.map((element) => JSON.stringify(element));
      arr.forEach((element) => {
        this.responseData.push(JSON.parse(element));
      });
      this.editedData = data.value.data;
    },
    cancel() {
      this.editedData = [];
      const arr = this.responseData.map((element) => JSON.stringify(element));
      arr.forEach((element) => {
        this.editedData.push(JSON.parse(element));
      });
    },
    clear() {
      this.responseData = [];
      this.editedData = [];
      this.impulsesEditable = false;
    },
    save() {
      const { $event } = useNuxtApp();
      this.isLoading = true;
      
      let diffArr = [];
      this.editedData.forEach((element, i) => {        
        if (
          element.impulse_value != null &&
          element.impulse_value != this.responseData[i].impulse_value
        ) {
          diffArr.push(element);
        }
      });    

      diffArr.forEach((element) => {
        apiFetch(`set_solver_task_param`, {
          value: {
            i_restrict_upper_type: element.restrict_upper_type,
            i_restrict_upper_value: element.restrict_upper_value,
            i_impulse_type: 1,
            i_restrict_lower_type: element.restrict_lower_type,
            i_restrict_lower_value: element.restrict_lower_value,
            i_impulse_dt: element.dt,
            i_indr_id: Number(element.indr_id),
            i_solver_task_id: Number(element.solver_task_id),
            i_impulse_value: element.impulse_value == '' ? null : Number(element.impulse_value),
            i_param_type: element.param_type == 3 ? 3 : 1,
            // i_param_type: Number(element.param_type),
          },
        });
      });
      
      this.impulsesEditable = false;
      this.responseData = [];
      this.editedData = [];
      
      setTimeout(() => {
        if (!this.impulsesEditable) {
          const { $i18n } = useNuxtApp();
          const t = $i18n.t;
          $event("solvers:update");
          this.isLoading = false;
          notification.open({
            message: t('store.impulses-success'),
            style: {
              border: "1px solid #b7eb8f",
              backgroundColor: "#f6ffed",
            },
          });
        }
      }, 2000);
    },
  },
});
